 atom_feed do |feed|
    feed.title("My great blog!")
    feed.updated(@campaigns[0].created_at) if @campaigns.length > 0

    @campaigns.each do |campaign|
      feed.entry(campaign) do |entry|
        entry.title(campaign.title)
        entry.content(campaign.description, type: 'html')

        entry.author do |author|
          author.name(campaign.owner.email)
        end
      end
    end
  end
