class Campaign < ActiveRecord::Base
  belongs_to :owner, class_name: 'User'
  validates :title, presence: true
  validates :description, presence: true
  validates :owner_id, presence: true
  validates :goal, presence: true

  def self.current
    where('expires_on > ?', Time.now)
  end

  def self.expired
    where('expires_on > ?', Time.now)
  end
end
