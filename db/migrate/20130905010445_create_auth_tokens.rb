class CreateAuthTokens < ActiveRecord::Migration
  def up
    User.find_each {|u| u.ensure_authentication_token!}
  end
  def down
  end
end
