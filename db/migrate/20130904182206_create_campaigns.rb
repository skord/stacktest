class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string :title
      t.text :description
      t.integer :goal
      t.integer :owner_id

      t.timestamps
    end
    add_index :campaigns, :owner_id
  end
end
