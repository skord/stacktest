class AddExpiresOnToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :expires_on, :datetime
  end
end
