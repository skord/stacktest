class CampaignsController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]
  def index
    per_page = params[:per_page] || 10
    @campaigns =  Campaign.includes(:owner).current.paginate(page: params[:page], per_page: per_page.to_i)
    expires_in 3.minutes, :public => true
    render stream: true
  end

  def show
    @campaign = Campaign.find(params[:id])
  end

  private

  def set_owned_campaign

  end

end
