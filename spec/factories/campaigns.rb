# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :campaign do
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraphs(3).join }
    goal { (1000..100000).to_a.sample }
    factory :current_campaign do
      expires_on { 1.week.from_now }
    end
    factory :expired_campaign do
      expires_on { 1.week.ago }
    end
  end
end
